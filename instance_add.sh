#!/bin/bash
#-----------------------------------------------------------------------------#
# Script for adding an instance
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#
# This script can be used to add multiple instances on one
# Ubuntu 18.04/Debian 9 server by setting different parameters for
# FL_USER, HTTP_PORT, LONGPOLLING_PORT and SERVER_NAME (if using PROXY_MODE)
#-----------------------------------------------------------------------------#
# Change directory to deployment folder:
# $  cd ~/flectra_deploy
# Make instance_add executable:
# $  sudo chmod +x instance_add.sh
# Execute instance_add:
# $  sudo ./instance_add.sh
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Parameters
#-----------------------------------------------------------------------------#
while true
do
  printf "\nPlease enter instance to add:\n"
  read INSTANCE
  if [ -f "./instances/$INSTANCE" ]; then
    printf "\nAre you sure you want to add $INSTANCE?\n Enter yes or no:\n"
    read CONFIRM
    if [ $CONFIRM = "yes" ]; then
      printf "\nAdding $FL_INSTANCE ...\n"
      . "./instances/$INSTANCE"
      break
    fi
  else
    printf "\nThe file $INSTANCE does not exist!!!\n"
    printf "Ensure it has been added to /flectra_deploy/instances\n"
  fi
done

#-----------------------------------------------------------------------------#
# Configure Firewall
#-----------------------------------------------------------------------------#
printf "\nConfiguring firewall for $FL_INSTANCE ...\n"
sudo ufw allow $HTTP_PORT/tcp
sudo ufw allow $LONGPOLLING_PORT/tcp

#-----------------------------------------------------------------------------#
# Add System User
#-----------------------------------------------------------------------------#
printf "\nAdding $FL_USER system user ...\n"
sudo adduser --system --quiet --shell=/bin/bash --home=$FL_HOME \
             --gecos $FL_USER --group $FL_USER

#-----------------------------------------------------------------------------#
# Add PostgreSQL User
#-----------------------------------------------------------------------------#
printf "\nAdding $DB_USER PostgreSQL user ...\n"
sudo su - postgres -c "createuser -dRS $DB_USER"

#-----------------------------------------------------------------------------#
# Setup Configuration
#-----------------------------------------------------------------------------#
printf "\nSetting up $FL_INSTANCE configuration ...\n"
. "./scripts/config_instance.sh"

#-----------------------------------------------------------------------------#
# Add Service
#-----------------------------------------------------------------------------#
printf "\nAdding $FL_INSTANCE systemd unit ...\n"
. "./scripts/systemd_unit.sh"

# Enable service
if [ $STARTUP_ENABLE = "True" ]; then
  printf "\nEnabling $FL_INSTANCE service ...\n"
  sudo systemctl enable $FL_INSTANCE
fi

#-----------------------------------------------------------------------------#
# Add Source and Modules
#-----------------------------------------------------------------------------#
printf "\nCreating $FL_INSTANCE directories ...\n"
sudo mkdir -p $FL_SERVER
sudo mkdir -p $FL_ADDONS_EXTRA
sudo mkdir -p $FL_ADDONS_CUSTOM
sudo mkdir /var/log/flectra/instance/$FL_INSTANCE

printf "\nAdding $FL_INSTANCE source ...\n"
sudo git clone --depth=1 --branch=$FL_VERSION \
  https://gitlab.com/flectra-hq/flectra.git $FL_SERVER

printf "\nAdding $FL_INSTANCE extra modules ...\n"
sudo git clone --depth=1 --branch=$FL_VERSION \
  https://gitlab.com/flectra-hq/extra-addons.git $FL_ADDONS_EXTRA

printf "\nAdding $FL_INSTANCE custom modules ...\n"
sudo rsync -azhP ./custom_addons/ $FL_ADDONS_CUSTOM

printf "\nInstalling $FL_INSTANCE source external dependencies ...\n"
sudo su - $FL_USER -c "pip3 install --user vobject qrcode pyldap num2words"
sudo su - $FL_USER -c "pip3 install --user -r $FL_SERVER/requirements.txt"
#sudo su - $FL_USER -c "pip3 install --user -r $FL_SERVER/doc/requirements.txt"

#printf "Installing $FL_INSTANCE extra modules external dependencies ...\n"
#sudo su - $FL_USER -c "pip3 install --user -r $FL_ADDONS_EXTRA/requirements.txt"

printf "\nInstalling $FL_INSTANCE custom modules external dependencies ...\n"
sudo su - $FL_USER -c "pip3 install --user -r $FL_ADDONS_CUSTOM/requirements.txt"

printf "\nSetting permissions on $FL_INSTANCE directories ...\n"
sudo chown -R $FL_USER:$FL_USER $FL_HOME
sudo chown -R $FL_USER:$FL_USER /var/log/flectra/instance/$FL_INSTANCE

#-----------------------------------------------------------------------------#
# Schedule Flectra Updates
#-----------------------------------------------------------------------------#
if [ $I_CRON_ENABLE = "True" ]; then
  printf "\nScheduling $FL_INSTANCE updates ...\n"
  . "./scripts/cron_instance.sh"
fi

#-----------------------------------------------------------------------------#
# Add Nginx Server Block
#-----------------------------------------------------------------------------#
if [ $PROXY_MODE = "True" ]; then
  if [ $LETSENCRYPT = "Obtain" ]; then
    printf "\nObtaining SSL certificate from Let's Encrypt ...\n"
    # Stop Nginx before running Certbot
    sudo systemctl stop nginx
    if  [ $LETSENCRYPT_TYPE = "PRODUCTION" ]; then
      sudo certbot certonly --standalone -n -d $SSL_DOMAINS
    else
      sudo certbot certonly --staging --standalone -n -d $SSL_DOMAINS
    fi
    sudo certbot renew --dry-run
    # Start Nginx after running Certbot
    sudo systemctl start nginx
    # SSL cert and key from Let's Encrypt
    SSL_CERT="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/fullchain.pem"
    SSL_CERT_KEY="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/privkey.pem"
    SSL_TRUST_CERT="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/chain.pem"
  elif [ $LETSENCRYPT = "Reuse" ]; then
    printf "\nReusing SSL certificate from Let's Encrypt ...\n"
    SSL_CERT="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/fullchain.pem"
    SSL_CERT_KEY="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/privkey.pem"
    SSL_TRUST_CERT="/etc/letsencrypt/live/$SSL_DOMAIN_MAIN/chain.pem"
  else
    printf "\nUsing SSL certificate from instance ...\n"
    sudo rsync -azhP ./ssl/certs/ $SSL_CERT_PATH
    sudo rsync -azhP ./ssl/private/ $SSL_CERT_KEY_PATH
  fi

  printf "\nSetting up Nginx server block ...\n"
  . "./scripts/config_nginx.sh"
fi

#-----------------------------------------------------------------------------#
# Start Service
#-----------------------------------------------------------------------------#
printf "\nStarting $FL_INSTANCE service ...\n"
sudo systemctl start $FL_INSTANCE

#-----------------------------------------------------------------------------#
# Instance Added!
#-----------------------------------------------------------------------------#
. "./scripts/message_info.sh"

cat <<-EOF

------------------------------------------------------------------

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
============== ADMIN PASSWORD ==============

$ADMIN_PASSWD

============== ADMIN PASSWORD ==============
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

------------------------------------------------------------------
Finished! Your $FL_INSTANCE has been added!
------------------------------------------------------------------
Best Regards,
Nicholas :)
------------------------------------------------------------------
EOF
