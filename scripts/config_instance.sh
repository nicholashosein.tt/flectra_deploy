#-----------------------------------------------------------------------------#
# Script for setting up instance configuration
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

# Create instance configuration
cat <<-EOF > /etc/flectra/instance/${FL_INSTANCE}.conf
[options]
admin_passwd = $ADMIN_PASSWD
db_host = $DB_HOST
db_port = $DB_PORT
db_user = $DB_USER
db_password = $DB_PASSWORD
dbfilter = $DBFILTER
list_db = $LIST_DB
addons_path = $FL_ADDONS,$FL_ADDONS_EXTRA,$FL_ADDONS_CUSTOM
http_port = $HTTP_PORT
longpolling_port = $LONGPOLLING_PORT
logfile = /var/log/flectra/instance/$FL_INSTANCE/${FL_INSTANCE}.log
limit_memory_hard = $LIMIT_MEMORY_HARD
limit_memory_soft = $LIMIT_MEMORY_SOFT
limit_request = $LIMIT_REQUEST
limit_time_cpu = $LIMIT_TIME_CPU
limit_time_real = $LIMIT_TIME_REAL
max_cron_threads = $MAX_CRON_THREADS
workers = $WORKERS
proxy_mode = $PROXY_MODE
EOF

# Secure instance configuration file
sudo chown $FL_USER:$FL_USER /etc/flectra/instance/${FL_INSTANCE}.conf
sudo chmod 640 /etc/flectra/instance/${FL_INSTANCE}.conf
