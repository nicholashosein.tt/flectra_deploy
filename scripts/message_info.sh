#-----------------------------------------------------------------------------#
# Script for displaying instance information
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

cat <<-EOF

------------------------------------------------------------------
Finished! Your $FL_INSTANCE is up and running!
------------------------------------------------------------------

Database management:         /web/database/manager
Accessible from IP:          $ADMIN_IP

------------------------ Specifications: -------------------------
Http Port:                   $HTTP_PORT
Longpolling Port:            $LONGPOLLING_PORT
System User:                 $FL_USER
PostgreSQL User:             $FL_USER
Source location:             $FL_SERVER
Extra addons location:       $FL_ADDONS_EXTRA
Custom addons location:      $FL_ADDONS_CUSTOM
Instance Flectra config:     /etc/flectra/instance/${FL_INSTANCE}.conf
Instance Flectra log:        /var/log/flectra/instance/$FL_INSTANCE/${FL_INSTANCE}.log
Instance systemd unit:       /lib/systemd/system/${FL_INSTANCE}.service
EOF

if [ $I_CRON_ENABLE = "True" ]; then
	cat <<-EOF
	Instance cron:               /opt/flectra/cron/${FL_INSTANCE}.sh
	Instance cron log:           /var/log/flectra/cron/${FL_INSTANCE}.log
	EOF
fi

if [ $PROXY_MODE = "True" ]; then
	cat <<-EOF
	Instance Nginx config:       /etc/nginx/sites-available/$FL_INSTANCE
	Instance Nginx access log:   /var/log/flectra/nginx/${FL_INSTANCE}.access.log
	Instance Nginx error log:    /var/log/flectra/nginx/${FL_INSTANCE}.error.log
	EOF
fi

cat <<-EOF
--------------------------- Journals: ----------------------------
Database journal:       sudo journalctl -u postgresql
EOF

if [ $PROXY_MODE = "True" ]; then
	cat <<-EOF
	Reverse proxy journal:  sudo journalctl -u nginx
	EOF
fi

cat <<-EOF
Server journal:         sudo journalctl -u $FL_INSTANCE

----------------------------- Usage: -----------------------------
Start Flectra service:
    sudo systemctl start $FL_INSTANCE      OR
    sudo service $FL_INSTANCE start
Stop Flectra service:
    sudo systemctl stop $FL_INSTANCE       OR
    sudo service $FL_INSTANCE stop
Restart Flectra service:
    sudo systemctl restart $FL_INSTANCE    OR
    sudo service $FL_INSTANCE restart
View Flectra service status:
    sudo systemctl status $FL_INSTANCE     OR
    sudo service $FL_INSTANCE status
EOF
