#-----------------------------------------------------------------------------#
# Script for creating server update cron job
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

# Create cron script
cat <<-EOF > /opt/flectra/cron/server.sh
#!/bin/bash
#-----------------------------------------------------------------------------#
# Server update and restart cron job
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
#-----------------------------------------------------------------------------#

# Set PATH environment variable
PATH=/usr/bin:/bin:/usr/sbin:/sbin

# Update server
/usr/bin/apt update
/usr/bin/apt upgrade -y
/usr/bin/apt autoremove -y

# Update pip3
/usr/bin/pip3 install -U pip

# Reboot server
/sbin/shutdown -r now
EOF

# Make cron script executable
sudo chmod +x /opt/flectra/cron/server.sh

# Create cron job
S_CRON_CMD="/opt/flectra/cron/server.sh"
S_CRON_JOB="$S_MINUTE $S_HOUR $S_DAY_OF_MONTH $S_MONTH $S_DAY_OF_WEEK $S_CRON_CMD"
S_CRON_LOG="/var/log/flectra/cron/server.log"
S_CRON_FULL="$S_CRON_JOB > $S_CRON_LOG"

sudo crontab -u root -l 2>/dev/null |
  sudo fgrep -i -v "$S_CRON_CMD" |
  { sudo cat; sudo echo "$S_CRON_FULL"; } |
  sudo crontab -u root -
