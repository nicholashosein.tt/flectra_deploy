#-----------------------------------------------------------------------------#
# Script for setting up instance systemd unit
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

# Create instance systemd unit
cat <<-EOF > /lib/systemd/system/${FL_INSTANCE}.service
[Unit]
Description=Flectra - The Open Source ERP and CRM For Your Business
Requires=postgresql.service
After=network.target postgresql.service

[Service]
Type=simple
PermissionsStartOnly=true
SyslogIdentifier=$FL_INSTANCE
User=$FL_USER
Group=$FL_USER
ExecStart=$FL_SERVER/flectra-bin --config=/etc/flectra/instance/${FL_INSTANCE}.conf
Restart=always
WorkingDirectory=$FL_SERVER/
StandardOutput=journal+console

[Install]
WantedBy=multi-user.target
EOF

# Secure instance systemd unit
sudo chown root: /lib/systemd/system/${FL_INSTANCE}.service
sudo chmod 755 /lib/systemd/system/${FL_INSTANCE}.service
