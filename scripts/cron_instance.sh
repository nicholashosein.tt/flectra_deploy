#-----------------------------------------------------------------------------#
# Script for creating instance update cron job
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

# Create cron script
cat <<-EOF > /opt/flectra/cron/${FL_INSTANCE}.sh
#!/bin/bash
#-----------------------------------------------------------------------------#
# $FL_INSTANCE update cron job
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
#-----------------------------------------------------------------------------#

# Set PATH environment variable
PATH=/usr/bin:/bin:/usr/sbin:/sbin

# Stop $FL_INSTANCE service
/bin/systemctl stop $FL_INSTANCE

# Update $FL_INSTANCE source
cd $FL_SERVER
/usr/bin/git fetch origin $FL_VERSION
/usr/bin/git reset --hard origin/$FL_VERSION
/bin/su - $FL_USER -c "pip3 install --user -r $FL_SERVER/requirements.txt"
#/bin/su - $FL_USER -c "pip3 install --user -r $FL_SERVER/doc/requirements.txt"

# Update $FL_INSTANCE extra modules
cd $FL_ADDONS_EXTRA
/usr/bin/git fetch origin $FL_VERSION
/usr/bin/git reset --hard origin/$FL_VERSION
#/bin/su - $FL_USER -c "pip3 install --user -r $FL_ADDONS_EXTRA/requirements.txt"

# Setting permissions on $FL_USER folder
/bin/chown -R $FL_USER:$FL_USER $FL_HOME

# Start $FL_INSTANCE service
/bin/systemctl start $FL_INSTANCE
EOF

# Make cron script executable
sudo chmod +x /opt/flectra/cron/${FL_INSTANCE}.sh

# Create cron job
I_CRON_CMD="/opt/flectra/cron/${FL_INSTANCE}.sh"
I_CRON_JOB="$I_MINUTE $I_HOUR $I_DAY_OF_MONTH $I_MONTH $I_DAY_OF_WEEK $I_CRON_CMD"
I_CRON_LOG="/var/log/flectra/cron/${FL_INSTANCE}.log"
I_CRON_FULL="$I_CRON_JOB > $I_CRON_LOG"

sudo crontab -u root -l 2>/dev/null |
  sudo fgrep -i -v "$I_CRON_CMD" |
  { sudo cat; sudo echo "$I_CRON_FULL"; } |
  sudo crontab -u root -
