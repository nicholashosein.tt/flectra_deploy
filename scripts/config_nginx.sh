#-----------------------------------------------------------------------------#
# Script for setting up instance Nginx server block
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

# Create Nginx config file
cat <<-EOF > /etc/nginx/sites-available/$FL_INSTANCE
#-----------------------------------------------------------------------------#
# $FL_INSTANCE Nginx server block
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
#-----------------------------------------------------------------------------#
# SSL generator:
# https://mozilla.github.io/server-side-tls/ssl-config-generator/
#-----------------------------------------------------------------------------#

# $FL_USER backend server
upstream $FL_USER {
  server 127.0.0.1:$HTTP_PORT;
}

# ${FL_USER}-longpolling backend server
upstream ${FL_USER}-longpolling {
  server 127.0.0.1:$LONGPOLLING_PORT;
}

# Proxy timeouts
proxy_read_timeout 720s;
proxy_connect_timeout 720s;
proxy_send_timeout 720s;

# Add Headers for $FL_USER proxy mode
proxy_set_header X-Forwarded-Host \$host;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto \$scheme;
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header Host \$http_host;
add_header X-Frame-Options SAMEORIGIN;
add_header X-Content-Type-Options nosniff;

# Max file size upload (0 for no limit)
client_max_body_size $CLIENT_MAX_BODY_SIZE;

# Logs
access_log /var/log/flectra/nginx/${FL_INSTANCE}.access.log;
error_log /var/log/flectra/nginx/${FL_INSTANCE}.error.log;

# HTTP server
server {
  listen 80;
  listen [::]:80;

  server_name $SERVER_NAME;
EOF

if [ $SSL_ENABLE = "True" ]; then
  if [ $POS_REDIRECT = "True" ]; then
    # HTTP server
		cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

		  # Redirect requests to $FL_USER backend server (fallback)
		  location / {
		    proxy_pass http://$FL_USER;
		  }

		  # Redirect longpolling requests to ${FL_USER}-longpolling backend server (fallback)
		  location = /longpolling/poll {
		    proxy_pass http://${FL_USER}-longpolling;
		  }

		  # Redirect non-POS and related content in HTTPS to allow POS in HTTP
		  location ~* ^(?!/pos/|/web/binary/company_logo|/?[^/]*/website/action/|/web/webclient/|/web/content/|/web_editor/static/|/web/css/|/web/static/|/point_of_sale/static).*\$ {
		    if (\$request_method != POST) {
		      return 301 https://\$host\$request_uri;
		    }
		    if (\$request_method = POST) {
		      proxy_pass http://$FL_USER;
		    }
		  }
		}
		EOF
  else
    # HTTP server
		cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

		  # Redirect HTTP -> HTTPS
		  return 301 https://\$host\$request_uri;
		}
		EOF
  fi

  # HTTPS server
	cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

	# HTTPS server
	server {
	  listen 443 ssl http2;
	  listen [::]:443 ssl http2;

	  server_name $SERVER_NAME;

	  # SSL parameters
	  ssl_certificate $SSL_CERT;
	  ssl_certificate_key $SSL_CERT_KEY;
	  ssl_session_timeout 1d;
	  ssl_session_cache shared:SSL:50m;
	  ssl_session_tickets off;

	  # Modern SSL configuration
	  ssl_protocols TLSv1.2;
	  ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
	  ssl_prefer_server_ciphers on;

	  # OCSP stapling ---
	  # Fetch OCSP records from URL in ssl_certificate and cache them
	  ssl_stapling on;
	  ssl_stapling_verify on;

	  # Verify chain of trust of OCSP response using Root CA and Intermediate certs
	  ssl_trusted_certificate $SSL_TRUST_CERT;

	  # IP DNS resolver
	  resolver $IP_DNS_RESOLVER;
	EOF

  if [ $POS_REDIRECT = "True" ]; then
    # HTTPS server
		cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

		  # Redirect POS requests HTTPS -> HTTP
		  location ~ ^/pos/web {
		    return 301 http://\$host:80\$request_uri;
		  }
		EOF
  else
    # HTTPS server
		cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

		  # HSTS (ngx_http_headers_module is required) (15768000 seconds = 6 months)
		  add_header Strict-Transport-Security max-age=15768000;
		EOF
  fi

  # HTTPS server
	cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

	  # Redirect requests to $FL_USER backend server
	  location / {
	    proxy_pass http://$FL_USER;
	  }

	  # Redirect longpolling requests to ${FL_USER}-longpolling backend server
	  location /longpolling {
	    proxy_pass http://${FL_USER}-longpolling;
	  }

	  # Deny database management to all but admin IP
	  location ~ ^/web/database {
	    allow $ADMIN_IP;
	    deny all;
	    proxy_pass http://$FL_USER;
	  }

	  # Common gzip
	  gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
	  gzip on;
	}
	EOF
else
  # HTTP server
	cat <<-EOF >> /etc/nginx/sites-available/$FL_INSTANCE

	  # Redirect requests to $FL_USER backend server
	  location / {
	    proxy_pass http://$FL_USER;
	  }

	  # Redirect longpolling requests to ${FL_USER}-longpolling backend server
	  location = /longpolling {
	    proxy_pass http://${FL_USER}-longpolling;
	  }

	  # Deny database management to all but admin IP
	  location ~ ^/web/database {
	    allow $ADMIN_IP;
	    deny all;
	    proxy_pass http://$FL_USER;
	  }

	  # Common gzip
	  gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
	  gzip on;
	}
	EOF
fi

# Enable instance Nginx server block
sudo ln -s /etc/nginx/sites-available/$FL_INSTANCE /etc/nginx/sites-enabled/
sudo systemctl reload nginx
