#!/bin/bash
#-----------------------------------------------------------------------------#
# Script for updating an instance
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#
# This script can be used to update an instance and add custom modules
#-----------------------------------------------------------------------------#
# Change directory to deployment folder:
# $  cd ~/flectra_deploy
# Make instance_update executable:
# $  sudo chmod +x instance_update.sh
# Execute instance_update:
# $  sudo ./instance_update.sh
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Parameters
#-----------------------------------------------------------------------------#
while true
do
  printf "\nPlease enter instance to update:\n"
  read INSTANCE
  if [ -f "./instances/$INSTANCE" ]; then
    printf "\nAre you sure you want to update $INSTANCE?\n Enter yes or no:\n"
    read CONFIRM
    if [ $CONFIRM = "yes" ]; then
      printf "\nUpdating $FL_INSTANCE ...\n"
      . "./instances/$INSTANCE"
      break
    fi
  else
    printf "\nThe file $INSTANCE does not exist!!!\n"
    printf "Ensure it has been added to /flectra_deploy/instances\n"
  fi
done

#-----------------------------------------------------------------------------#
# Stop Service
#-----------------------------------------------------------------------------#
printf "\nStopping $FL_INSTANCE service ...\n"
sudo systemctl stop $FL_INSTANCE

#-----------------------------------------------------------------------------#
# Add Custom Modules
#-----------------------------------------------------------------------------#
printf "\nAdding $FL_INSTANCE custom modules ...\n"
sudo rsync -azhP ./custom_addons/ $FL_ADDONS_CUSTOM
sudo su - $FL_USER -c "pip3 install --user -r $FL_ADDONS_CUSTOM/requirements.txt"

#-----------------------------------------------------------------------------#
# Update Source and Modules
#-----------------------------------------------------------------------------#
printf "\nUpdating $FL_INSTANCE source ...\n"
cd $FL_SERVER
sudo git fetch origin $FL_VERSION
sudo git reset --hard origin/$FL_VERSION
sudo su - $FL_USER -c "pip3 install --user -r $FL_SERVER/requirements.txt"
#sudo su - $FL_USER -c "pip3 install --user -r $FL_SERVER/doc/requirements.txt"

printf "\nUpdating $FL_INSTANCE extra modules ...\n"
cd $FL_ADDONS_EXTRA
sudo git fetch origin $FL_VERSION
sudo git reset --hard origin/$FL_VERSION
#sudo su - $FL_USER -c "pip3 install --user -r $FL_ADDONS_EXTRA/requirements.txt"

#-----------------------------------------------------------------------------#
# Set Permissions
#-----------------------------------------------------------------------------#
printf "\nSetting permissions on $FL_INSTANCE directories ...\n"
sudo chown -R $FL_USER:$FL_USER $FL_HOME

#-----------------------------------------------------------------------------#
# Start Service
#-----------------------------------------------------------------------------#
printf "\nStarting $FL_INSTANCE service ...\n"
sudo systemctl start $FL_INSTANCE

#-----------------------------------------------------------------------------#
# Instance Updated!
#-----------------------------------------------------------------------------#
. "./scripts/message_info.sh"

cat <<-EOF

------------------------------------------------------------------
Finished! Your $FL_INSTANCE has been updated!
------------------------------------------------------------------
Best Regards,
Nicholas :)
------------------------------------------------------------------

EOF
