# Introduction

This project contains a number of scripts to simplify the task of deploying
Flectra in a live environment. It is a bit overkill for basic installations
where you may only need to evaluate Flectra.

## Main Features

* Configure Firewall
* Install Dependencies
* Setup service
* Setup reverse proxy
* Setup SSL encryption
* Schedule updates

## Instances

![Deployment Architecture](/static/architecture.png)

You can manage multiple instances on a single Ubuntu 18.04/Debian 9 server.
An instance is basically the source used to run Flectra as well as any custom
requirements needed. See the figure above.

As you may have noticed, every instance and resources listed in the white
boxes are isolated and/or different from each other.

The resources shown in the blue layer are shared by all instances. If you
desire to also isolate this layer per instance, use of a Docker would be a
better alternative to this project.

The red layer shows that this project can install instances on both Ubuntu
18.04 and Debian 9 servers.

# Usage

1. Login to server with a non-root user with sudo privileges

2. Change directory to user home folder
```shell
cd ~/
```

3. Clone project
```shell
sudo apt install git -y
git clone --depth=1 --branch=master https://gitlab.com/nicholashosein.tt/flectra_deploy.git
```

## Prepare server

1. Change directory to deployment folder
```shell
cd ~/flectra_deploy
```

2. Review and adjust parameters file
```shell
nano parameters_server
```

3. Make prepare_server.sh executable
```shell
sudo chmod +x prepare_server.sh
```

4. Execute prepare_server.sh
```shell
sudo ./prepare_server.sh
```

## Add an instance

1. Change directory to deployment folder and update deployment
```shell
cd ~/flectra_deploy
git pull
```

2. Create instance file and place into the **instances** folder. A sample
instance file called **flectra** has been provided. Use it as the basis for
other instances. Ensure different parameters are set for the parameters
FL_USER, HTTP_PORT, LONGPOLLING_PORT and SERVER_NAME (if using PROXY_MODE).
```shell
cd instances
cp flectra new_instance
nano new_instance
```

3. Place custom addons (if any) into the custom_addons folder and add
external pip dependencies (if any) to requirements.txt

4. Place SSL certificates in ssl/certs folder and SSL key in ssl/private
folder. The ssl/generate folder contain a README.md file on generating
a self signed certificate and private key. The entire ssl directory can
be ignored if obtaining SSL certificates from Let's Encrypt.

5. Make instance_add.sh executable
```shell
cd ~/flectra_deploy
sudo chmod +x instance_add.sh
```

6. Execute instance_add.sh
```shell
sudo ./instance_add.sh
```

7. Enter name of instance file you created e.g **flectra**

## Update an instance

1. Change directory to deployment folder and update deployment
```shell
cd ~/flectra_deploy
git pull
```

2. Place custom addons (if any) into the custom_addons folder and add
external pip dependencies (if any) to requirements.txt

3. Make instance_update.sh executable
```shell
cd ~/flectra_deploy
sudo chmod +x instance_update.sh
```

4. Execute instance_update.sh
```shell
sudo ./instance_update.sh
```

5. Enter name of instance e.g **flectra**

## Remove an instance

1. Delete all databases created for the instance (from database manager)

2. Change directory to deployment folder and update deployment
```shell
cd ~/flectra_deploy
git pull
```

3. Make instance_remove.sh executable
```shell
sudo chmod +x instance_remove.sh
```

4. Execute instance_remove.sh
```shell
sudo ./instance_remove.sh
```

5. Enter name of instance e.g **flectra**

# NOTE

Postfix has to manually be configured as an SMTP relay.

*Author:* Nicholas Hosein
*Contact:* nicholashosein.tt@gmail.com
