# Steps used to generate self signed certificate

1. Create **self.conf** configuration file used for prompting field values
```shell
nano ~/flectra_deploy/ssl/generate/self.conf
```

2. Create 1 year SSL certificate and key using **self.conf**
```shell
openssl req -x509 -days 365 -nodes -newkey rsa:2048 \
        -config ~/flectra_deploy/ssl/generate/self.conf \
        -keyout ~/flectra_deploy/ssl/private/self_key.pem \
        -out ~/flectra_deploy/ssl/certs/self_cert.pem
```

3. Examine the **self-cert.pem** SSL certificate
```shell
openssl x509 -in ~/flectra_deploy/ssl/certs/self_cert.pem -text -noout
```

4. Create PKCS#12 file using **self_key.pem** and **self_cert.pem** (optional)
```shell
openssl pkcs12 -export -nodes \
        -in ~/flectra_deploy/ssl/certs/self_cert.pem \
        -inkey ~/flectra_deploy/ssl/private/self_key.pem \
        -out ~/flectra_deploy/ssl/certs/self_cert.p12 \
        -name self_cert_p12
```

*Author:* Nicholas Hosein
*Contact:* nicholashosein.tt@gmail.com
