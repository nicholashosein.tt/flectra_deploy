#!/bin/bash
#-----------------------------------------------------------------------------#
# Script for preparing Ubuntu 18.04/Debian 9 server
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#
# Change directory to deployment folder:
# $  cd ~/flectra_deploy
# Make prepare_server executable:
# $  sudo chmod +x prepare_server.sh
# Execute prepare_server:
# $  sudo ./prepare_server.sh
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Parameters
#-----------------------------------------------------------------------------#
. "./parameters_server"

#-----------------------------------------------------------------------------#
# Install Dependencies
#-----------------------------------------------------------------------------#
printf "\nInstalling dependencies ...\n"
sudo apt update
sudo apt install postgresql build-essential python3-pil python3-lxml \
                 python-ldap3 python3-dev python3-pip python3-setuptools \
                 libjpeg-dev libldap2-dev libsasl2-dev libxml2-dev \
                 libxslt1-dev gdebi ufw -y
if [ $SERVER_OS = "DEBIAN" ]; then
  sudo apt install postgresql-server-dev-9.6 \
                   curl software-properties-common -y
  curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
  sudo apt install nodejs
else
  sudo apt install postgresql-server-dev-10 nodejs npm -y
fi

printf "\nInstalling less CSS via nodejs ...\n"
sudo npm install -g less@3.0.4 less-plugin-clean-css -y

printf "\nInstalling GeoLite City ...\n"
wget -N https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz
sudo tar -xzf GeoLite2-City.tar.gz
sudo mkdir /usr/share/GeoIP/
sudo mv GeoLite2-City* /usr/share/GeoIP/
sudo find . -name "GeoLite2-City*" -exec rm -r {} \;

printf "\nUpdating pip3 ...\n"
sudo -H pip3 install -U pip

#-----------------------------------------------------------------------------#
# Install libpng12
#-----------------------------------------------------------------------------#
printf "\nInstalling libpng12-0 ...\n"
if [ `getconf LONG_BIT` = "64" ]; then
  wget http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_amd64.deb
else
  wget http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1.1_i386.deb
fi
sudo dpkg -i libpng12-0*
sudo find . -name "libpng12-0*" -exec rm -r {} \;

#-----------------------------------------------------------------------------#
# Install Wkhtmltopdf
#-----------------------------------------------------------------------------#
printf "\nInstalling Wkhtmltopdf ...\n"
# WKHTMLTOPDF download links
if [ $SERVER_OS = "DEBIAN" ]; then
  # Debian 9
  WKHTMLTOPDF_amd64="https://builds.wkhtmltopdf.org/0.12.1.3/wkhtmltox_0.12.1.3-1~stretch_amd64.deb"
  WKHTMLTOPDF_i386="https://builds.wkhtmltopdf.org/0.12.1.3/wkhtmltox_0.12.1.3-1~stretch_i386.deb"
else
  # Ubuntu 18.04
  WKHTMLTOPDF_amd64="https://downloads.wkhtmltopdf.org/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-amd64.deb"
  WKHTMLTOPDF_i386="https://downloads.wkhtmltopdf.org/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-trusty-i386.deb"
fi
if [ `getconf LONG_BIT` = "64" ]; then
  WK_URL=$WKHTMLTOPDF_amd64
else
  WK_URL=$WKHTMLTOPDF_i386
fi
wget $WK_URL
sudo gdebi --n `basename $WK_URL`
sudo find . -name "*wkhtml*" -exec rm -r {} \;
sudo ln -s /usr/local/bin/wkhtmltopdf /usr/bin
sudo ln -s /usr/local/bin/wkhtmltoimage /usr/bin
sudo apt-mark hold wkhtmltopdf

#-----------------------------------------------------------------------------#
# Install Nginx
#-----------------------------------------------------------------------------#
if [ $NGINX = "True" ]; then
  printf "\nInstalling Nginx ...\n"
  sudo apt install nginx -y
fi

#-----------------------------------------------------------------------------#
# Install Certbot
#-----------------------------------------------------------------------------#
if [ $CERTBOT = "True" ]; then
  printf "\nInstalling Certbot ...\n"
  if [ $SERVER_OS = "DEBIAN" ]; then
    # Debian 9
    sudo apt install certbot -y
  else
    # Ubuntu 18.04
    sudo add-apt-repository universe
    sudo add-apt-repository ppa:certbot/certbot -y
    sudo apt update
    sudo apt install certbot -y
  fi
fi

#-----------------------------------------------------------------------------#
# Install Postfix
#-----------------------------------------------------------------------------#
if [ $POSTFIX = "True" ]; then
  printf "\nInstalling Postfix ...\n"
  sudo debconf-set-selections <<< "postfix postfix/mailname string your.hostname.com"
  sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
  sudo DEBIAN_FRONTEND=noninteractive apt install postfix -y
fi

#-----------------------------------------------------------------------------#
# Set Timezone
#-----------------------------------------------------------------------------#
printf "\nSetting timezone to $SERVER_TZ ...\n"
sudo timedatectl set-timezone $SERVER_TZ

#-----------------------------------------------------------------------------#
# Configure Firewall
#-----------------------------------------------------------------------------#
printf "\nConfiguring Firewall ...\n"
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw --force enable

#-----------------------------------------------------------------------------#
# Create Flectra Directories
#-----------------------------------------------------------------------------#
printf "\nCreating Flectra directories ...\n"
sudo mkdir /opt/flectra
sudo mkdir /opt/flectra/cron
sudo mkdir /opt/flectra/instance

printf "\nCreating log directories ...\n"
sudo mkdir /var/log/flectra
sudo mkdir /var/log/flectra/cron
sudo mkdir /var/log/flectra/instance
sudo mkdir /var/log/flectra/nginx

printf "\nCreating config directories ...\n"
sudo mkdir /etc/flectra
sudo mkdir /etc/flectra/instance

#-----------------------------------------------------------------------------#
# Schedule Server Update and Restart
#-----------------------------------------------------------------------------#
if [ $S_CRON_ENABLE = "True" ]; then
  printf "\nScheduling server updates and restarts ...\n"
  . "./scripts/cron_server.sh"
fi

#-----------------------------------------------------------------------------#
# Update Ubuntu 18.04/Debian 9
#-----------------------------------------------------------------------------#
printf "\nUpdating $SERVER_OS ...\n"
sudo apt upgrade -y
sudo apt autoremove -y

#-----------------------------------------------------------------------------#
# Restart Server
#-----------------------------------------------------------------------------#
printf "\nRestarting $SERVER_OS ...\n"
sudo shutdown -r now
