#-----------------------------------------------------------------------------#
# File containing parameters used by instance_*.sh scripts
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Common
#-----------------------------------------------------------------------------#
# System User
FL_USER="flectra"

# Flectra version
FL_VERSION="1.0"

# Instance
FL_INSTANCE="${FL_USER}_server"

# Instance paths
FL_HOME="/opt/flectra/instance/$FL_USER"
FL_SERVER="$FL_HOME/$FL_INSTANCE"
FL_ADDONS="$FL_SERVER/addons"
FL_ADDONS_EXTRA="$FL_HOME/extra/addons"
FL_ADDONS_CUSTOM="$FL_HOME/custom/addons"

# Set to true to start instance on startup
STARTUP_ENABLE="True"

#-----------------------------------------------------------------------------#
# Flectra Configuration
#-----------------------------------------------------------------------------#
# Database operations password
ADMIN_PASSWD=$(openssl rand -base64 32)

# PostgreSQL database
DB_HOST="False"
DB_PORT="False"
DB_USER=$FL_USER
DB_PASSWORD="False"

# Database filter
#DBFILTER="^(?i)%d$"
DBFILTER="False"

# List databases
LIST_DB="True"

# Ports
HTTP_PORT="7073"
LONGPOLLING_PORT="7072"

# Number of CPU cores
CPU_NO="2"

# Amount of RAM in MB
RAM_AMT="2048"

# Server operations
LIMIT_MEMORY_HARD=$(( $RAM_AMT * 1000000 * 95/100 ))
LIMIT_MEMORY_SOFT=$(( $LIMIT_MEMORY_HARD * 85/100 ))
LIMIT_REQUEST="8196"
LIMIT_TIME_CPU="480"
LIMIT_TIME_REAL="960"
MAX_CRON_THREADS="1"
WORKERS=$(( $CPU_NO * 2 ))

# Proxy mode (set to True to utilize Nginx reverse proxying)
PROXY_MODE="False"

#-----------------------------------------------------------------------------#
# Instance Update Schedule
#-----------------------------------------------------------------------------#
# Enable scheduled instance updates
I_CRON_ENABLE="True"

# Set schedule
#----------------------------#
# Day of week (0-7)(Sun=0/7) #
# Month (1-12)               #
# Day of month (1-31)        #
# Hour (0-23)                #
# Minute (0-59)              #
#----------------------------#
# For further information    #
# $  man 5 crontab           #
#----------------------------#
# Mon-Fri @ 23:00
I_DAY_OF_WEEK="1-5"
I_MONTH="*"
I_DAY_OF_MONTH="*"
I_HOUR="23"
I_MINUTE="00"

#-----------------------------------------------------------------------------#
# Nginx (Proxy and SSL) Configuration
#-----------------------------------------------------------------------------#
# Domain(s)
SERVER_NAME="example.com www.example.com eg2.org eg3.net etc.com"

# Deny database management to all but admin IP ("all" to allow all IPs)
ADMIN_IP="all"

# Max file size upload ("0" for no limit)
CLIENT_MAX_BODY_SIZE="0"

# Enable SSL encryption
SSL_ENABLE="True"

# Redirect POS requests HTTPS -> HTTP
POS_REDIRECT="True"

# IP DNS resolver
IP_DNS_RESOLVER="8.8.8.8"

# SSL cert and key (not utilized if using Let's Encrypt)
SSL_CERT_PATH="/etc/ssl/certs"
SSL_CERT_KEY_PATH="/etc/ssl/private"
SSL_CERT="$SSL_CERT_PATH/self_cert.pem"
SSL_CERT_KEY="$SSL_CERT_KEY_PATH/self_key.pem"

# Trusted CA certificates (PEM format) (not utilized if using Let's Encrypt)
SSL_TRUST_CERT="$SSL_CERT_PATH/self_cert.pem"

# Remove certificate from instance (not utilized if using Let's Encrypt)
REMOVE_INSTANCE_CERT="False"

# Obtain/Reuse SSL certificate from Let's Encrypt
LETSENCRYPT="False"
#LETSENCRYPT="Obtain"
#LETSENCRYPT="Reuse"
LETSENCRYPT_TYPE="STAGING"
#LETSENCRYPT_TYPE="PRODUCTION"

# Remove SSL certificate from Let's Encrypt
REMOVE_LETSENCRYPT_CERT="False"

# Domain(s) to be encrypted by Let's Encrypt.
# When reusing, SSL_DOMAIN_MAIN must be set to the main domain name used when
# originally obtaining from Let's Encrypt.
SSL_DOMAIN_MAIN="example.com"
SSL_DOMAIN_WILDCARD=*.${SSL_DOMAIN_MAIN}
SSL_DOMAINS="$SSL_DOMAIN_MAIN,$SSL_DOMAIN_WILDCARD,eg2.org,eg3.net,etc.com"
