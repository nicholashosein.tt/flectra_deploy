#!/bin/bash
#-----------------------------------------------------------------------------#
# Script for removing an instance
# Author: Nicholas Hosein
# Contact: nicholashosein.tt@gmail.com
# Licensed under: GNU Affero General Public License
#-----------------------------------------------------------------------------#
# This script can be used to remove an instance
# Ensure to delete all databases created for the instance before executing
#-----------------------------------------------------------------------------#
# Change directory to deployment folder:
# $  cd ~/flectra_deploy
# Make instance_remove executable:
# $  sudo chmod +x instance_remove.sh
# Execute instance_remove:
# $  sudo ./instance_remove.sh
#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# Parameters
#-----------------------------------------------------------------------------#
while true
do
  printf "\nPlease enter instance to remove:\n"
  read INSTANCE
  if [ -f "./instances/$INSTANCE" ]; then
    printf "\nAre you sure you want to remove $INSTANCE?\n Enter yes or no:\n"
    printf "\nEnsure to delete all databases created for $INSTANCE before continuing!!!\n"
    read CONFIRM
    if [ $CONFIRM = "yes" ]; then
      printf "\nRemoving $FL_INSTANCE ...\n"
      . "./instances/$INSTANCE"
      break
    fi
  else
    printf "\nThe file $INSTANCE does not exist!!!\n"
    printf "Ensure it has been added to /flectra_deploy/instances\n"
  fi
done

#-----------------------------------------------------------------------------#
# Stop Service
#-----------------------------------------------------------------------------#
printf "\nStopping $FL_INSTANCE service ...\n"
sudo systemctl stop $FL_INSTANCE

#-----------------------------------------------------------------------------#
# Deconfigure Firewall
#-----------------------------------------------------------------------------#
printf "\nDeconfiguring firewall for $FL_INSTANCE ...\n"
sudo ufw deny $HTTP_PORT/tcp
sudo ufw deny $LONGPOLLING_PORT/tcp

#-----------------------------------------------------------------------------#
# Remove System User
#-----------------------------------------------------------------------------#
printf "\nRemoving $FL_USER system user ...\n"
sudo deluser --remove-all-files $FL_USER

#-----------------------------------------------------------------------------#
# Remove PostgreSQL User
#-----------------------------------------------------------------------------#
printf "\nRemoving $DB_USER PostgreSQL user ...\n"
sudo su - postgres -c "dropuser $DB_USER"

#-----------------------------------------------------------------------------#
# Remove Service
#-----------------------------------------------------------------------------#
printf "\nRemoving $FL_INSTANCE systemd unit ...\n"
sudo rm /lib/systemd/system/${FL_INSTANCE}.service
sudo systemctl daemon-reload

#-----------------------------------------------------------------------------#
# Deschedule Flectra Updates
#-----------------------------------------------------------------------------#
if [ $I_CRON_ENABLE = "True" ]; then
  printf "\nDescheduling $FL_INSTANCE updates ...\n"
  sudo rm /opt/flectra/cron/${FL_INSTANCE}.sh

  sudo crontab -u root -l 2>/dev/null |
    sudo fgrep -i -v "/opt/flectra/cron/${FL_INSTANCE}.sh" |
    sudo crontab -u root -
fi

#-----------------------------------------------------------------------------#
# Remove Nginx Server Block
#-----------------------------------------------------------------------------#
if [ $PROXY_MODE = "True" ]; then
  if [ $REMOVE_LETSENCRYPT_CERT = "True" ]; then
    printf "\nRevoking SSL certificate from Let's Encrypt ...\n"
    if  [ $LETSENCRYPT_TYPE = "PRODUCTION" ]; then
      sudo certbot revoke --cert-path /etc/letsencrypt/live/$SSL_DOMAIN_MAIN/cert.pem
      sudo certbot delete --cert-name $SSL_DOMAIN_MAIN
    else
      sudo certbot revoke --staging --cert-path /etc/letsencrypt/live/$SSL_DOMAIN_MAIN/cert.pem
      sudo certbot delete --cert-name $SSL_DOMAIN_MAIN
    fi
  else
    if [ $REMOVE_INSTANCE_CERT = "True" ]; then
    printf "\nRemoving instance certificate ...\n"
    sudo rm $SSL_CERT
    sudo rm $SSL_CERT_KEY
    sudo rm $SSL_TRUST_CERT
  fi

  printf "\nRemoving Nginx server block ...\n"
  sudo rm /etc/nginx/sites-enabled/$FL_INSTANCE
  sudo rm /etc/nginx/sites-available/$FL_INSTANCE
  sudo systemctl reload nginx
fi

#-----------------------------------------------------------------------------#
# Instance Removed!
#-----------------------------------------------------------------------------#
cat <<-EOF

------------------------------------------------------------------
Finished! Your $FL_INSTANCE has been removed!
------------------------------------------------------------------
Best Regards,
Nicholas :)
------------------------------------------------------------------

EOF
